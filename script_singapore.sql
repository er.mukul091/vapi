USE [master]
GO
/****** Object:  Database [SKVRDB_UAT]    Script Date: 2/2/2022 8:20:28 PM ******/
CREATE DATABASE [SKVRDB_UAT]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SKVRDB_UAT', FILENAME = N'F:\Data_00\SKVRDB_UAT.mdf' , SIZE = 65536KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SKVRDB_UAT_log', FILENAME = N'F:\TLog_00\SKVRDB_UAT_log.ldf' , SIZE = 65536KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SKVRDB_UAT] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SKVRDB_UAT].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SKVRDB_UAT] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET ARITHABORT OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SKVRDB_UAT] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SKVRDB_UAT] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SKVRDB_UAT] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SKVRDB_UAT] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SKVRDB_UAT] SET  MULTI_USER 
GO
ALTER DATABASE [SKVRDB_UAT] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SKVRDB_UAT] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SKVRDB_UAT] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SKVRDB_UAT] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SKVRDB_UAT] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SKVRDB_UAT] SET QUERY_STORE = OFF
GO
USE [SKVRDB_UAT]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [SKVRDB_UAT]
GO
/****** Object:  User [AD-BAYER-CNB\EIISG]    Script Date: 2/2/2022 8:20:28 PM ******/
CREATE USER [AD-BAYER-CNB\EIISG] FOR LOGIN [AD-BAYER-CNB\EIISG] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5]    Script Date: 2/2/2022 8:20:28 PM ******/
CREATE USER [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5] FOR LOGIN [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5]
GO
/****** Object:  DatabaseRole [db_executor]    Script Date: 2/2/2022 8:20:28 PM ******/
CREATE ROLE [db_executor]
GO
ALTER ROLE [db_datareader] ADD MEMBER [AD-BAYER-CNB\EIISG]
GO
ALTER ROLE [db_executor] ADD MEMBER [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5]
GO
ALTER ROLE [db_datareader] ADD MEMBER [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [AD-BAYER-CNB\bs.a.SQL.AP.usr-dev.BYSG015CMK8.INPD5]
GO
/****** Object:  Schema [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3]    Script Date: 2/2/2022 8:20:28 PM ******/
CREATE SCHEMA [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3]
GO
/****** Object:  UserDefinedFunction [dbo].[ModulesName]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ModulesName](
    @Roleid INT
    
)
RETURNS nvarchar(max)
AS 
BEGIN
declare @ModulesName nvarchar(max)
     SELECT @ModulesName=COALESCE(@ModulesName + ',', '') + CAST(page_name AS nVARCHAR(max))
      FROM tbl_assign_pages inner join tbl_pages on pageid=pid where roleid=@Roleid
	return @ModulesName
END;
GO
/****** Object:  Table [dbo].[Login_Logs]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Login_Logs](
	[Logid] [bigint] IDENTITY(1,1) NOT NULL,
	[Userid] [bigint] NULL,
	[LoginTime] [datetime] NULL,
	[logoutTime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Login] PRIMARY KEY CLUSTERED 
(
	[Logid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_acc_popups]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_acc_popups](
	[popup] [nvarchar](max) NULL,
	[masterid] [bigint] NULL,
	[rowid] [bigint] IDENTITY(1,1) NOT NULL,
	[pcount] [int] NULL,
 CONSTRAINT [PK_tbl_acc_popups] PRIMARY KEY CLUSTERED 
(
	[rowid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_analytics]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_analytics](
	[aid] [bigint] IDENTITY(1,1) NOT NULL,
	[module] [nvarchar](500) NULL,
	[logid] [bigint] NULL,
	[Pop_Ups] [nvarchar](max) NULL,
	[userid] [bigint] NULL,
	[k_Time_hours] [int] NULL,
	[k_Time_min] [int] NULL,
	[Addeddate] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_analytics] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Answers]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Answers](
	[Ansid] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NULL,
	[isactive] [bit] NULL,
	[Answer] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_Answers] PRIMARY KEY CLUSTERED 
(
	[Ansid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_app_links]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_app_links](
	[Linkid] [int] IDENTITY(1,1) NOT NULL,
	[LinkTitle] [nvarchar](50) NULL,
	[LinkUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_app_links] PRIMARY KEY CLUSTERED 
(
	[Linkid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_assign_pages]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_assign_pages](
	[aid] [int] IDENTITY(1,1) NOT NULL,
	[roleid] [int] NULL,
	[pageid] [int] NULL,
 CONSTRAINT [PK_tbl_assign_pages] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Error_Log]    Script Date: 2/2/2022 8:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Error_Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Method_Name] [nvarchar](max) NULL,
	[Page_Name] [nvarchar](max) NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_tbl_Error_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_pages]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_pages](
	[pid] [int] IDENTITY(1,1) NOT NULL,
	[Page_Name] [nvarchar](max) NULL,
	[Isactive] [bit] NULL,
 CONSTRAINT [PK_tbl_pages] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_popups]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_popups](
	[ID] [nvarchar](255) NULL,
	[PopupName] [nvarchar](max) NULL,
	[F3] [nvarchar](255) NULL,
	[F4] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_questions]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_questions](
	[Qid] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](500) NULL,
	[Isactive] [bit] NULL,
 CONSTRAINT [PK_tbl_questions] PRIMARY KEY CLUSTERED 
(
	[Qid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ratings]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ratings](
	[Rid] [bigint] IDENTITY(1,1) NOT NULL,
	[userid] [bigint] NULL,
	[rating] [int] NULL,
	[Qid] [int] NULL,
	[additional_comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_ratings] PRIMARY KEY CLUSTERED 
(
	[Rid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[R_Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
	[Isactive] [bit] NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[R_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[F_Name] [nvarchar](max) NULL,
	[RoleId] [int] NULL,
	[DOB] [datetime] NULL,
	[EmailId] [nvarchar](50) NULL,
	[Mobile_No] [nvarchar](50) NULL,
	[Password] [nvarchar](150) NULL,
	[Isactive] [bit] NULL,
	[OTP] [nvarchar](10) NULL,
	[Role] [nvarchar](50) NULL,
	[L_Name] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[Token] [nvarchar](max) NULL,
	[addeddate] [datetime] NULL,
	[_ExpiryTime] [datetime] NULL,
	[Days] [int] NULL,
	[Hours] [int] NULL,
	[Min] [int] NULL,
	[CWID] [nvarchar](50) NULL,
	[UserType] [int] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_user_exp]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_user_exp](
	[qid] [bigint] IDENTITY(1,1) NOT NULL,
	[moduleId] [int] NULL,
	[Isliked] [int] NULL,
	[userid] [bigint] NULL,
	[addeddate] [datetime] NULL,
 CONSTRAINT [PK_tbl_user_exp] PRIMARY KEY CLUSTERED 
(
	[qid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_users_answers]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_users_answers](
	[rowid] [bigint] IDENTITY(1,1) NOT NULL,
	[userid] [bigint] NULL,
	[qid] [int] NULL,
	[ansid] [int] NULL,
	[addeddate] [datetime] NULL,
 CONSTRAINT [PK_tbl_users_answers] PRIMARY KEY CLUSTERED 
(
	[rowid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_acc_popups] ON 

INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 24, 1, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 25, 2, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 26, 3, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 27, 4, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 28, 5, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 29, 6, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 30, 7, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 31, 8, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 32, 9, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 33, 10, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 34, 11, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 35, 12, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 36, 13, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 37, 14, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 38, 15, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 39, 16, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 40, 17, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 41, 18, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 42, 19, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 43, 20, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 44, 21, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 45, 22, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 46, 23, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 47, 24, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 48, 25, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 49, 26, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 50, 27, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 51, 28, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 52, 29, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 54, 30, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 53, 31, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 55, 32, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 56, 33, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 57, 34, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 58, 35, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 59, 36, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 60, 37, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 62, 38, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 61, 39, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 63, 40, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 64, 41, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 65, 42, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 66, 43, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 67, 44, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 68, 45, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 69, 46, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 70, 47, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 71, 48, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 72, 49, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 73, 50, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 74, 51, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 75, 52, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 76, 53, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 77, 54, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 78, 55, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 79, 56, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 80, 57, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 81, 58, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 82, 59, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 83, 60, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 84, 61, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 85, 62, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 86, 63, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 87, 64, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 88, 65, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 89, 66, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 90, 67, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 91, 68, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 92, 69, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 93, 70, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 94, 71, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 95, 72, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 96, 73, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 97, 74, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 98, 75, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 99, 76, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 100, 77, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 101, 78, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 102, 79, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 103, 80, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 104, 81, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 105, 82, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 106, 83, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 107, 84, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 108, 85, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 109, 86, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 110, 87, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 111, 88, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 112, 89, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 113, 90, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 114, 91, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 115, 92, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 116, 93, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 117, 94, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 118, 95, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 119, 96, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 120, 97, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 121, 98, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 122, 99, 1)
GO
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 123, 100, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 124, 101, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 125, 102, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 126, 103, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 127, 104, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 128, 105, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 129, 106, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 130, 107, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 131, 108, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 132, 109, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 133, 110, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 134, 111, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 135, 112, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 136, 113, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 137, 114, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 138, 115, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 139, 116, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 140, 117, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 141, 118, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 142, 119, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 143, 120, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 144, 121, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 145, 122, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 146, 123, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 147, 124, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 148, 125, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 149, 126, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 150, 127, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 151, 128, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 152, 129, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 153, 130, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 154, 131, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 155, 132, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 156, 133, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 157, 134, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 158, 135, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 159, 136, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 160, 137, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 161, 138, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 162, 139, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 163, 140, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 164, 141, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 165, 142, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 166, 143, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 167, 144, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 168, 145, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 169, 146, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 170, 147, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 171, 148, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 172, 149, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 173, 150, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 174, 151, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 175, 152, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 176, 153, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 177, 154, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 178, 155, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 179, 156, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 180, 157, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 181, 158, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 182, 159, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 183, 160, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 184, 161, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 185, 162, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 186, 163, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 187, 164, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 188, 165, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 189, 166, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 190, 167, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 191, 168, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 192, 169, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 193, 170, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 194, 171, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 195, 172, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 196, 173, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 197, 174, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 198, 175, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 199, 176, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 200, 177, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 201, 178, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 202, 179, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 203, 180, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 204, 181, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 205, 182, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 206, 183, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 207, 184, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 208, 185, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 209, 186, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 210, 187, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 211, 188, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 212, 189, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 213, 190, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 214, 191, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 215, 192, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 216, 193, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 217, 194, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 218, 195, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 219, 196, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 220, 197, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 221, 198, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 222, 199, 1)
GO
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 223, 200, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 224, 201, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 225, 202, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 226, 203, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 227, 204, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 228, 205, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 229, 206, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 230, 207, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 231, 208, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 232, 209, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 233, 210, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 234, 211, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 235, 212, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 236, 213, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 237, 214, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 238, 215, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 239, 216, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 240, 217, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 241, 218, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 242, 219, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 243, 220, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 244, 221, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 245, 222, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 246, 223, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 247, 224, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 248, 225, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 249, 226, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 250, 227, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 251, 228, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 252, 229, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 253, 230, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 254, 231, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 255, 232, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 256, 233, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 257, 234, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 258, 235, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 259, 236, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 260, 237, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 261, 238, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 262, 239, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 263, 240, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 264, 241, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 265, 242, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 266, 243, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 267, 244, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 268, 245, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 269, 246, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 270, 247, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 271, 248, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 272, 249, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 273, 250, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 274, 251, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 275, 252, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 276, 253, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 277, 254, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 278, 255, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 279, 256, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 280, 257, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 281, 258, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 282, 259, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 283, 260, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 284, 261, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 285, 262, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 286, 263, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 287, 264, 6)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 288, 265, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 289, 266, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 290, 267, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 291, 268, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 292, 269, 7)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 293, 270, 8)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 294, 271, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 295, 272, 6)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 296, 273, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 297, 274, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 298, 275, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 299, 276, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 300, 277, 1)
SET IDENTITY_INSERT [dbo].[tbl_acc_popups] OFF
SET IDENTITY_INSERT [dbo].[tbl_Answers] ON 

INSERT [dbo].[tbl_Answers] ([Ansid], [QuestionId], [isactive], [Answer]) VALUES (1, 2, 1, N'this is answer')
SET IDENTITY_INSERT [dbo].[tbl_Answers] OFF
SET IDENTITY_INSERT [dbo].[tbl_app_links] ON 

INSERT [dbo].[tbl_app_links] ([Linkid], [LinkTitle], [LinkUrl]) VALUES (1, N'VT Mobile Application', N'http://10.26.1.73/VirtualTour/AnroidApp/Bayer_VT_UATv01.apk')
INSERT [dbo].[tbl_app_links] ([Linkid], [LinkTitle], [LinkUrl]) VALUES (2, N'VT HMD Application', N'www.facebook.com')
INSERT [dbo].[tbl_app_links] ([Linkid], [LinkTitle], [LinkUrl]) VALUES (3, N'VT Desktop Application', N'http://10.26.1.73/\/irtualTour/Desktop/Bayer_UAT_Version0.1.zip')
SET IDENTITY_INSERT [dbo].[tbl_app_links] OFF
SET IDENTITY_INSERT [dbo].[tbl_assign_pages] ON 

INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (27, 9, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (28, 9, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (29, 9, 6)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (30, 10, 7)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (31, 10, 8)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (41, 8, 2)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (42, 8, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (43, 4, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (44, 4, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (45, 4, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (46, 1, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (47, 1, 2)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (48, 1, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (49, 1, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (50, 1, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (51, 1, 6)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (52, 1, 7)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (53, 1, 8)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (54, 1, 9)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (55, 5, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (56, 5, 2)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (57, 5, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (58, 5, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (59, 5, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (60, 5, 6)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (61, 5, 9)
SET IDENTITY_INSERT [dbo].[tbl_assign_pages] OFF
SET IDENTITY_INSERT [dbo].[tbl_Error_Log] ON 

INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (1, N'An error occurred while executing the command definition. See the inner exception for details.', N'GetUser', N'LoginMaster', CAST(N'2021-04-14T23:40:35.800' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (2, N'An error occurred while executing the command definition. See the inner exception for details.', N'GetUser', N'LoginMaster', CAST(N'2021-04-14T23:41:09.203' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (3, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:35:58.157' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (4, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:36:17.060' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (5, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:41:42.427' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (6, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:50:42.613' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (7, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:50:43.107' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (8, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:56:50.737' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (9, N'SOme error Occur.Please try again', N'LoginController', N'LoginMaster', CAST(N'2022-01-21T00:01:52.153' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Error_Log] OFF
SET IDENTITY_INSERT [dbo].[tbl_pages] ON 

INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (2, N'Qc/AIMI', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (3, N'Engineering Store', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (4, N'OHC', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (5, N'HSE', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (6, N'Canteen', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (7, N'GMP', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (8, N'CMA', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (9, N'Vr Road Tour', 1)
SET IDENTITY_INSERT [dbo].[tbl_pages] OFF
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop1', N'Leed Certificate', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop2', N'Kiosk', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop3', N'Lay / Malhar conference room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop4 ', N'The cafeteria', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop5', N'The longue/ Santoor Conference room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop1', N'HPLC - ', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop2 ', N' Air Genertaor



 
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop3', N'Left inst (3 big one) - Nitrogen Generator', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop4', N'Auto tritrator', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop5', N'UV spectrometer', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop6', N'Density meter', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop7', N'microscope', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop8', N'GCMS -Gas Chromotograph with Mass spectrometer', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop9', N'GCHS - GC Head Space', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop10', N'Printer type inst.- auto circulator for controoling the temp at auto reactor', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop11', N'20L Jacketed Reactor
10L Jacketed Reactor
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop12', N'Centrifuge for Acidic / Alkaline filtration', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop13', N'Auto reactor -', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop14', N'Hydrogen generator
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop15', N'Auto circulator for controlling the temperature at autoreactor
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop16', N'High pressure reactor', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop17', N'Millipore: - The most advanced pure water system
•           Weighing Balance: - METTLER TOLEDO Advanced Analytical Balance
•           HPLC: - High performance liquid chromatography
•           Mastersizer: - Particle size analyzer
•           Kinexus: -', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'canteen_pop1', N'RFID sensor for capturing Lunch & Dinner ', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'canteen_pop2', N'. Roti making area, 2. cooking area. 3. Tea making area 4. Dish wash area. 5 Salad making area', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'canteen_pop3', N'rice boiler, dishwasher machine, dough kneader machine, palversier machine, Grinding machine, Potato pillar machine idli making machine, cold and dry storage.', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'Eng_pop1', N'Total Area of Engineering Spare part warehouse is 50.5 X 25.5 X 10 meters(LBH) (1250 Sq.mtr', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'Eng_pop2', N'Aisle in which  Crane travel on rail track to pick and store the Storage Bin.
- Shuttle car is the vehicle who delivers the bin received from crane to IN / OUT station through conveyors. shuttle car is with PLC system and integrated with WMS.
- Cranes', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'Eng_pop3', N'The Crane travels on rail track to pick or to store the bins.
- Shuttle car is the vehicle which delivers the bin received from crane to IN / OUT station through conveyors. Shuttle car is also connected to the PLC system and integrated with WMS.', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop1', N'Audiometry Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop2', N'AED Machine', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop3', N'Suction Machine - OHC/  Ambulance', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop4', N'Oxygen Cylinder - OHC/  Ambulance/ Plants', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop5', N'ECG Machine - OHC Recovery Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop6', N'Needle Syringe Destroyer - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop7', N'Instrument Sterilizer - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop8', N'Autoclave Machine - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop9', N'Water Distiller - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop1', N'Cardiac Monitor - Ambulance', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop2', N'Autoloded Stretcher', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop3', N'Spinal Stretcher ', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop4', N'WheelChair', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop1', N'Fire Hosepipes', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop2', N'75 kg tank Dry-chemical powder', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop3', N'2 Nos. 22.5 kg CO2 extinguisher', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop4', N'Fog branch & Jumbo', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop5', N'fire-bucket', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop6', N'Fireman Axe', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop7', N'SCBA-Set', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop8', N'Microchem suit 3000 & 5000', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_questions] ON 

INSERT [dbo].[tbl_questions] ([Qid], [Question], [Isactive]) VALUES (1, N'Rate our App', NULL)
INSERT [dbo].[tbl_questions] ([Qid], [Question], [Isactive]) VALUES (2, N'this is question', 1)
SET IDENTITY_INSERT [dbo].[tbl_questions] OFF
SET IDENTITY_INSERT [dbo].[tbl_ratings] ON 

INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (1, 12, 5, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (2, 12, 4, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (3, 12, 3, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (4, 12, 3, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_ratings] OFF
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (4, N'Induction', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (5, N'Internal Bayer Employee', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (6, N'Vendor', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (8, N'Auditor', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (9, N'Student', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (10, N'Marketing Customer', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (11, N'Government officer', 1)
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
SET IDENTITY_INSERT [dbo].[tbl_User] ON 

INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (1, N'Admin', 1, NULL, NULL, NULL, N'nmjn@12#hmk12$lkvnc', NULL, NULL, NULL, NULL, N'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (8, N'Deepak', 2, NULL, N'deepak@airvlabs.com', N'9166474449', NULL, 1, NULL, NULL, N'Singh', NULL, N'cmFMKcZiZPIdBDYycfH+Qw==', CAST(N'2021-04-22T10:43:39.263' AS DateTime), CAST(N'2021-05-16T15:46:18.997' AS DateTime), 0, 10, 5, NULL, 0)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (12, N'mukul', 2, NULL, N'er.mukul09@gmail.com', N'9782316861', N'mkpassword', 1, NULL, NULL, N'sharma', N'mksharma', NULL, CAST(N'2021-04-29T12:58:43.457' AS DateTime), CAST(N'2021-05-09T07:28:44.060' AS DateTime), 10, 0, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (14, N'deepak', 3, NULL, N'deepak491singh@gmail.com', N'9784567654', N'123456', 1, NULL, NULL, NULL, N'dpk', N'8/n5eanmDoA3Qe+F0rdG1g==', CAST(N'2021-04-30T00:48:36.187' AS DateTime), CAST(N'2021-04-30T05:18:36.187' AS DateTime), 0, 10, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (17, N'Visitor1', 9, NULL, N'chinmay@airvlabs.com', N'1234567891', N'visitor', 1, NULL, NULL, N'1', N'Visit', N'fnq4tQq+R7uqhs3RLncl+g==', CAST(N'2021-04-30T14:13:09.717' AS DateTime), CAST(N'2021-04-30T08:53:09.717' AS DateTime), 0, 0, 10, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (26, N'Vikram', 5, NULL, N'vikram.doke@bayer.com', N'1199228833', NULL, 1, NULL, NULL, N'Doke', NULL, NULL, CAST(N'2021-05-11T09:17:33.440' AS DateTime), CAST(N'8133-04-05T03:47:33.440' AS DateTime), 2232326, 0, 0, N'EWDKV', NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (27, N'vik', 5, NULL, N'vikram_pal123@live.com', N'1232123232', N'vikramdoke123', 1, NULL, NULL, N'dok', N'vikramdoke1', NULL, CAST(N'2021-05-11T09:25:46.083' AS DateTime), CAST(N'2021-05-31T03:55:46.083' AS DateTime), 20, 0, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (28, N'kk', 5, NULL, N'gorikaran0@gmail.com', N'1121123444', N'kg123', 1, NULL, NULL, N'gg', N'kg', NULL, CAST(N'2021-05-11T09:52:29.590' AS DateTime), CAST(N'2021-05-31T04:22:29.590' AS DateTime), 20, 0, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (29, N'Krunal', 1, NULL, N'krunal.prajapati@bayer.com', N'1122334451', NULL, 1, NULL, NULL, N'Prajapati', NULL, N'm7Q6EVMrNASreJYHcyVZ7g==', CAST(N'2021-05-12T12:07:40.120' AS DateTime), CAST(N'2022-05-12T06:37:40.120' AS DateTime), 365, 0, 0, N'MBPIK', NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (30, N'ssd', 1, NULL, N'e.rmukul09@gmail.com', N'8989898989', NULL, 1, NULL, NULL, N'sdsd', NULL, NULL, CAST(N'2021-05-16T10:48:02.430' AS DateTime), CAST(N'2021-05-26T07:38:53.367' AS DateTime), 10, 0, 0, NULL, 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (32, N'AAAB', 5, NULL, N'AAAB@GMAIL.COM', N'1232343456', NULL, 1, NULL, NULL, N'AAAB', NULL, NULL, CAST(N'2021-05-20T13:32:24.953' AS DateTime), CAST(N'2021-06-04T08:05:02.573' AS DateTime), 15, 0, 0, NULL, 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (33, N'Deepak', 1, NULL, N'deepak.singh3.ext@bayer.com', N'1122112211', NULL, 1, NULL, NULL, N'Singh', NULL, NULL, CAST(N'2021-05-24T11:29:27.040' AS DateTime), CAST(N'2022-05-24T05:59:27.040' AS DateTime), 365, 0, 0, N'EIIMD', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (41, N'Janhavi', 4, NULL, N'janhavi.dewoolkar.ext@bayer.com', N'2225311996', NULL, 1, NULL, NULL, N'Dewoolkar', NULL, NULL, CAST(N'2021-06-03T19:32:55.317' AS DateTime), CAST(N'2021-06-04T14:02:55.317' AS DateTime), 1, 0, 0, N'EMUYV', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (45, N'Karan', 1, NULL, N'karan.gori.ext@bayer.com', N'1121121121', NULL, 1, NULL, NULL, N'Gori', NULL, N'Nfco6jXgIzkaePtNEQEGlw==', CAST(N'2021-06-11T16:07:46.577' AS DateTime), CAST(N'2022-06-21T13:45:47.410' AS DateTime), 364, 10, 0, N'EICYY', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (46, N'HSP', 10, NULL, N'hsp.nagashayanam@bayer.com', N'1212134343', NULL, 1, NULL, NULL, N'Nagashyanam', NULL, N'YrTS66nP+Oa5TSsN8dlhWw==', CAST(N'2021-07-02T15:38:40.187' AS DateTime), CAST(N'2022-07-23T09:56:46.420' AS DateTime), 365, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (47, N'Nilesh', 10, NULL, N'nilesh.jain.ext@bayer.com', N'4444455555', N'NJ1111', 1, NULL, NULL, N'Jain', N'Nilesh', N'jSn1pZWP4MXZqylIRUMHyQ==', CAST(N'2021-07-23T15:29:11.617' AS DateTime), CAST(N'2022-07-23T09:59:11.617' AS DateTime), 365, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (49, N'Rahul', 4, NULL, N'rahul.singh@bayer.com', N'1111122222', N'RS1111', 1, NULL, NULL, N'Singh', N'Rahul', NULL, CAST(N'2021-07-23T15:38:39.420' AS DateTime), CAST(N'2021-08-02T10:08:39.420' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (50, N'Preshit', 1, NULL, N'gori1998karan@gmail.com', N'8320775510', N'prprprpr', 1, NULL, NULL, N'Rathod', N'prpr', NULL, CAST(N'2021-09-30T15:07:05.200' AS DateTime), CAST(N'2021-10-03T09:37:05.200' AS DateTime), 3, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (51, N'Testuser1', 1, NULL, N'testuser1@mailinator.com', N'123456789', N'test@123', 1, NULL, NULL, NULL, N'testuser1', NULL, CAST(N'2021-10-14T00:22:54.600' AS DateTime), CAST(N'2021-10-23T18:52:54.600' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (52, N'Testuser2', 8, NULL, N'testuser2@mailinator.com', N'987654321', N'testuser@123', 1, NULL, NULL, NULL, N'testuser2', NULL, CAST(N'2021-10-14T00:24:39.330' AS DateTime), CAST(N'2021-10-23T18:54:39.330' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (53, N'testuser3', 9, NULL, N'testuser3@mailinator.com', N'987654123', N'testuser@123', 1, NULL, NULL, NULL, N'testuser3', N'7JAX9NibJ52LapjFvLOB5g==', CAST(N'2021-10-14T00:25:53.540' AS DateTime), CAST(N'2021-10-23T18:55:53.540' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (54, N'Testuser123', 1, NULL, N'test@mailinator.com', N'789456123', N'test@123', 1, NULL, NULL, NULL, N'test@123', NULL, CAST(N'2021-10-14T12:50:59.720' AS DateTime), CAST(N'2021-10-24T07:20:59.720' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (55, N'test', 1, NULL, N'test@test.com', N'9090909090', N'tester', 1, NULL, NULL, N'testr', N'tester', NULL, CAST(N'2022-01-23T19:56:48.263' AS DateTime), CAST(N'2022-02-01T14:26:48.263' AS DateTime), 9, 0, 0, NULL, 2)
SET IDENTITY_INSERT [dbo].[tbl_User] OFF
/****** Object:  StoredProcedure [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3].[GetRatingAnalytics]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3].[GetRatingAnalytics]
@qid int
as
begin
select count(rid) 'Count',rating from tbl_ratings where qid=@qid group by rating
end


GO
/****** Object:  StoredProcedure [dbo].[GetAssignedData]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetAssignedData]
@roleid int=null
as
begin
declare @pages nvarchar(max)
if(isnull(@roleid,0)=0)
begin
select distinct(roleid),RoleName,isnull(dbo.[ModulesName](roleid),'') 'Modules' from tbl_assign_pages inner join tbl_pages on pageid=pid inner join tbl_Role on tbl_assign_pages.roleid=tbl_Role.r_id
end
else
begin
select distinct(roleid),RoleName,isnull(dbo.[ModulesName](roleid),'') 'Modules' from tbl_assign_pages inner join tbl_pages on pageid=pid inner join tbl_Role on tbl_assign_pages.roleid=tbl_Role.r_id where r_id=@roleid

end

end
GO
/****** Object:  StoredProcedure [dbo].[GetModuleVisits]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE Proc [dbo].[GetModuleVisits] 
@month int,
@year int  
as  
begin  
declare @totalcount float  
select @totalcount=count(aid)  from tbl_analytics   where  MONTH(cast(tbl_analytics.addeddate as date))=@month and  year(cast(tbl_analytics.addeddate as date))=@year
if(@totalcount>0)  
select Page_Name,convert(float,Round((count(aid)/@totalcount)*100,2) )'Count' from tbl_pages inner join tbl_analytics on module=Page_Name inner join tbl_User on tbl_User.id=userid
 where MONTH(cast(tbl_analytics.addeddate as date))=@month and  year(cast(tbl_analytics.addeddate as date))=@year group by Page_Name  
else  
select '' Page_Name,convert(float,'0') 'Count'   
  

end  

GO
/****** Object:  StoredProcedure [dbo].[getUserGrupedByName]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[getUserGrupedByName] 
@mname int,
@year int
as


begin
select count(id) 'Count',RoleName 'Month' from tbl_user inner join tbl_role on roleid=r_id where month(addeddate)=@mname and year(addeddate)=@year group by RoleName
end


GO
/****** Object:  StoredProcedure [dbo].[GetUserList]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserList]
AS
BEGIN
	select Id,F_Name,RoleId,DOB,EmailId,Mobile_No,Password,tbl_User.Isactive,OTP,cwid 'Role',L_Name,UserName,R_Id,RoleName,tbl_Role.Isactive 'Isactive1',UserType from tbl_User 
	inner join tbl_Role on tbl_Role.R_Id=tbl_User.RoleId
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetVisitedPopups]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetVisitedPopups] 
@aid bigint
as

begin
select popupname from tbl_popups inner join tbl_acc_popups on id=popup where masterid=@aid
end
GO
/****** Object:  StoredProcedure [dbo].[GetVisiting_Details]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[GetVisiting_Details] 
@userid bigint
as

begin
select aid,module,k_Time_days,k_Time_hours,k_Time_min,k_Time_sec,userid,isnull(addeddate,'') 'addeddate' from tbl_analytics where userid=@userid order by addeddate desc
end
GO
/****** Object:  StoredProcedure [dbo].[GetVisitingUsers]    Script Date: 2/2/2022 8:20:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetVisitingUsers]
as
begin
select distinct(userid) 'Id',f_Name,Mobile_No,EmailId,rolename from tbl_analytics inner join tbl_user on id=userid inner join tbl_Role on r_id=roleid
end
GO
USE [master]
GO
ALTER DATABASE [SKVRDB_UAT] SET  READ_WRITE 
GO
