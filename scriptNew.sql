USE [master]
GO
/****** Object:  Database [BVPL-VT]    Script Date: 1/29/2022 9:38:17 AM ******/
CREATE DATABASE [BVPL-VT]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'App_Data', FILENAME = N'E:\MSSQL13.APPD125\MSSQL\DATA\BVPL-VT.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'App_Data_log', FILENAME = N'E:\MSSQL13.APPD125\MSSQL\TLog\BVPL-VT_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BVPL-VT] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BVPL-VT].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BVPL-VT] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BVPL-VT] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BVPL-VT] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BVPL-VT] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BVPL-VT] SET ARITHABORT OFF 
GO
ALTER DATABASE [BVPL-VT] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BVPL-VT] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BVPL-VT] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BVPL-VT] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BVPL-VT] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BVPL-VT] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BVPL-VT] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BVPL-VT] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BVPL-VT] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BVPL-VT] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BVPL-VT] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BVPL-VT] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BVPL-VT] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BVPL-VT] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BVPL-VT] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BVPL-VT] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BVPL-VT] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BVPL-VT] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BVPL-VT] SET  MULTI_USER 
GO
ALTER DATABASE [BVPL-VT] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BVPL-VT] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BVPL-VT] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BVPL-VT] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BVPL-VT] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BVPL-VT] SET QUERY_STORE = OFF
GO
USE [BVPL-VT]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [BVPL-VT]
GO
/****** Object:  User [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125]    Script Date: 1/29/2022 9:38:17 AM ******/
CREATE USER [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125] FOR LOGIN [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125]
GO
/****** Object:  DatabaseRole [db_executor]    Script Date: 1/29/2022 9:38:17 AM ******/
CREATE ROLE [db_executor]
GO
ALTER ROLE [db_executor] ADD MEMBER [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125]
GO
ALTER ROLE [db_datareader] ADD MEMBER [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015qugb.appd125]
GO
/****** Object:  Schema [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3]    Script Date: 1/29/2022 9:38:17 AM ******/
CREATE SCHEMA [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3]
GO
/****** Object:  UserDefinedFunction [dbo].[ModulesName]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ModulesName](
    @Roleid INT
    
)
RETURNS nvarchar(max)
AS 
BEGIN
declare @ModulesName nvarchar(max)
     SELECT @ModulesName=COALESCE(@ModulesName + ',', '') + CAST(page_name AS nVARCHAR(max))
      FROM tbl_assign_pages inner join tbl_pages on pageid=pid where roleid=@Roleid
	return @ModulesName
END;

GO
/****** Object:  Table [dbo].[tbl_acc_popups]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_acc_popups](
	[popup] [nvarchar](max) NULL,
	[masterid] [bigint] NULL,
	[rowid] [bigint] IDENTITY(1,1) NOT NULL,
	[pcount] [int] NULL,
 CONSTRAINT [PK_tbl_acc_popups] PRIMARY KEY CLUSTERED 
(
	[rowid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_analytics]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_analytics](
	[aid] [bigint] IDENTITY(1,1) NOT NULL,
	[module] [nvarchar](500) NULL,
	[Count] [int] NULL,
	[k_Time_days] [int] NULL,
	[p_accessed] [int] NULL,
	[Pop_Ups] [nvarchar](max) NULL,
	[userid] [bigint] NULL,
	[k_Time_hours] [int] NULL,
	[k_Time_min] [int] NULL,
	[k_Time_sec] [int] NULL,
	[Addeddate] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_analytics] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_app_links]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_app_links](
	[Linkid] [int] IDENTITY(1,1) NOT NULL,
	[LinkTitle] [nvarchar](50) NULL,
	[LinkUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_app_links] PRIMARY KEY CLUSTERED 
(
	[Linkid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_assign_pages]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_assign_pages](
	[aid] [int] IDENTITY(1,1) NOT NULL,
	[roleid] [int] NULL,
	[pageid] [int] NULL,
 CONSTRAINT [PK_tbl_assign_pages] PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Error_Log]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Error_Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Method_Name] [nvarchar](max) NULL,
	[Page_Name] [nvarchar](max) NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_tbl_Error_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_pages]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_pages](
	[pid] [int] IDENTITY(1,1) NOT NULL,
	[Page_Name] [nvarchar](max) NULL,
	[Isactive] [bit] NULL,
 CONSTRAINT [PK_tbl_pages] PRIMARY KEY CLUSTERED 
(
	[pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_popups]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_popups](
	[ID] [nvarchar](255) NULL,
	[PopupName] [nvarchar](max) NULL,
	[F3] [nvarchar](255) NULL,
	[F4] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_questions]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_questions](
	[Qid] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_questions] PRIMARY KEY CLUSTERED 
(
	[Qid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ratings]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ratings](
	[Rid] [bigint] IDENTITY(1,1) NOT NULL,
	[userid] [bigint] NULL,
	[rating] [int] NULL,
	[Qid] [int] NULL,
	[additional_comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_ratings] PRIMARY KEY CLUSTERED 
(
	[Rid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Role]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Role](
	[R_Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NULL,
	[Isactive] [bit] NULL,
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[R_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[F_Name] [nvarchar](max) NULL,
	[RoleId] [int] NULL,
	[DOB] [datetime] NULL,
	[EmailId] [nvarchar](50) NULL,
	[Mobile_No] [nvarchar](50) NULL,
	[Password] [nvarchar](150) NULL,
	[Isactive] [bit] NULL,
	[OTP] [nvarchar](10) NULL,
	[Role] [nvarchar](50) NULL,
	[L_Name] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[Token] [nvarchar](max) NULL,
	[addeddate] [datetime] NULL,
	[_ExpiryTime] [datetime] NULL,
	[Days] [int] NULL,
	[Hours] [int] NULL,
	[Min] [int] NULL,
	[CWID] [nvarchar](50) NULL,
	[UserType] [int] NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_user_exp]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_user_exp](
	[qid] [bigint] IDENTITY(1,1) NOT NULL,
	[moduleId] [int] NULL,
	[Isliked] [int] NULL,
	[userid] [bigint] NULL,
	[addeddate] [datetime] NULL,
 CONSTRAINT [PK_tbl_user_exp] PRIMARY KEY CLUSTERED 
(
	[qid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_acc_popups] ON 

INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 24, 1, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 25, 2, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 26, 3, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 27, 4, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 28, 5, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 29, 6, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 30, 7, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 31, 8, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 32, 9, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 33, 10, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 34, 11, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 35, 12, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 36, 13, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 37, 14, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 38, 15, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 39, 16, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 40, 17, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 41, 18, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 42, 19, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 43, 20, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 44, 21, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 45, 22, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 46, 23, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 47, 24, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 48, 25, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 49, 26, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 50, 27, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 51, 28, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 52, 29, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 54, 30, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 53, 31, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 55, 32, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 56, 33, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 57, 34, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 58, 35, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 59, 36, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 60, 37, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 62, 38, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 61, 39, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 63, 40, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 64, 41, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 65, 42, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 66, 43, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 67, 44, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 68, 45, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 69, 46, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 70, 47, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 71, 48, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 72, 49, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 73, 50, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 74, 51, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 75, 52, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 76, 53, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 77, 54, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 78, 55, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 79, 56, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 80, 57, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 81, 58, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 82, 59, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 83, 60, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 84, 61, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 85, 62, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 86, 63, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 87, 64, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 88, 65, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 89, 66, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 90, 67, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 91, 68, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 92, 69, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 93, 70, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 94, 71, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 95, 72, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 96, 73, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 97, 74, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 98, 75, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 99, 76, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 100, 77, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 101, 78, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 102, 79, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 103, 80, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 104, 81, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 105, 82, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 106, 83, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 107, 84, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 108, 85, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 109, 86, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 110, 87, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 111, 88, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 112, 89, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 113, 90, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 114, 91, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 115, 92, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 116, 93, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 117, 94, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 118, 95, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 119, 96, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 120, 97, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 121, 98, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 122, 99, 1)
GO
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 123, 100, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 124, 101, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 125, 102, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 126, 103, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 127, 104, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 128, 105, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 129, 106, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 130, 107, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 131, 108, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 132, 109, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 133, 110, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 134, 111, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 135, 112, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 136, 113, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 137, 114, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 138, 115, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 139, 116, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 140, 117, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 141, 118, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 142, 119, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 143, 120, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 144, 121, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 145, 122, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 146, 123, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'Admin_pop1', 147, 124, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 148, 125, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 149, 126, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 150, 127, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 151, 128, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 152, 129, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 153, 130, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 154, 131, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 155, 132, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 156, 133, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 157, 134, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 158, 135, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 159, 136, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 160, 137, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 161, 138, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 162, 139, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 163, 140, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 164, 141, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 165, 142, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 166, 143, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 167, 144, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 168, 145, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 169, 146, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 170, 147, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 171, 148, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 172, 149, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 173, 150, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 174, 151, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 175, 152, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 176, 153, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 177, 154, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 178, 155, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 179, 156, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 180, 157, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 181, 158, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 182, 159, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 183, 160, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 184, 161, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 185, 162, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 186, 163, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 187, 164, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 188, 165, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 189, 166, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 190, 167, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 191, 168, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 192, 169, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 193, 170, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 194, 171, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 195, 172, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 196, 173, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 197, 174, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 198, 175, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 199, 176, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 200, 177, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 201, 178, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 202, 179, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 203, 180, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 204, 181, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 205, 182, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 206, 183, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 207, 184, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 208, 185, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 209, 186, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 210, 187, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 211, 188, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 212, 189, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 213, 190, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 214, 191, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 215, 192, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 216, 193, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 217, 194, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 218, 195, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 219, 196, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 220, 197, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 221, 198, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 222, 199, 1)
GO
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 223, 200, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 224, 201, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 225, 202, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 226, 203, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 227, 204, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 228, 205, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 229, 206, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 230, 207, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 231, 208, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 232, 209, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 233, 210, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 234, 211, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 235, 212, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 236, 213, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 237, 214, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 238, 215, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 239, 216, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 240, 217, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 241, 218, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 242, 219, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 243, 220, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 244, 221, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 245, 222, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 246, 223, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 247, 224, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 248, 225, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 249, 226, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 250, 227, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 251, 228, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 252, 229, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 253, 230, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 254, 231, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 255, 232, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 256, 233, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 257, 234, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 258, 235, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 259, 236, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 260, 237, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 261, 238, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 262, 239, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 263, 240, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 264, 241, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 265, 242, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 266, 243, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 267, 244, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 268, 245, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 269, 246, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 270, 247, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 271, 248, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 272, 249, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 273, 250, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 274, 251, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 275, 252, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 276, 253, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 277, 254, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 278, 255, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 279, 256, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 280, 257, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 281, 258, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 282, 259, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 283, 260, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 284, 261, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 285, 262, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 286, 263, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 287, 264, 6)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 288, 265, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 289, 266, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 290, 267, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 291, 268, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 292, 269, 7)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 293, 270, 8)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 294, 271, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 295, 272, 6)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 296, 273, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 297, 274, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 298, 275, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 299, 276, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 300, 277, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 301, 278, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 302, 279, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 303, 280, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 304, 281, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 305, 282, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 306, 283, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 307, 284, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 308, 285, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 309, 286, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 310, 287, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 311, 288, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 312, 289, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop2', 313, 290, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 314, 291, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop8', 315, 292, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 316, 293, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 317, 294, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 318, 295, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 319, 296, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 320, 297, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 321, 298, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop3', 322, 299, 1)
GO
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop9', 323, 300, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 324, 301, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 325, 302, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 326, 303, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 327, 304, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 328, 305, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop3', 329, 306, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 330, 307, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop6', 331, 308, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 332, 309, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 333, 310, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 334, 311, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 335, 312, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 336, 313, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 337, 314, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop1', 338, 315, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop8', 339, 316, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 340, 317, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 341, 318, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop4', 342, 319, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop5', 343, 320, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 344, 321, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 345, 322, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 346, 323, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 347, 324, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 348, 325, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 349, 326, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 350, 327, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 351, 328, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 352, 329, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 353, 330, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 354, 331, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 355, 332, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 356, 333, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 357, 334, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 358, 335, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 359, 336, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 360, 337, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop3', 361, 338, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 362, 339, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop1', 363, 340, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop8', 364, 341, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop6', 365, 342, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop9', 366, 343, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 367, 344, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 368, 345, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 369, 346, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 370, 347, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 371, 348, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 372, 349, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop1', 373, 350, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop8', 374, 351, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 375, 352, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 376, 353, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop1', 377, 354, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop4', 378, 355, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop2', 379, 356, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop2', 380, 357, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop4', 381, 358, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop6', 382, 359, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop7', 383, 360, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop6', 384, 361, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop5', 385, 362, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop4', 386, 363, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 387, 364, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 388, 365, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 389, 366, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 390, 367, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 391, 368, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 392, 369, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 393, 370, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop3', 394, 371, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 395, 372, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 396, 373, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 397, 374, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 398, 375, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 399, 376, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 400, 377, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 401, 378, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 402, 379, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 403, 380, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 404, 381, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 405, 382, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 406, 383, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 407, 384, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 408, 385, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop3', 409, 386, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop2', 410, 387, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop8', 411, 388, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop6', 412, 389, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop7', 413, 390, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop9', 414, 391, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 415, 392, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 416, 393, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 417, 394, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 418, 395, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 419, 396, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 420, 397, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop1', 421, 398, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 422, 399, 1)
GO
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 423, 400, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop4', 424, 401, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'amb_pop2', 425, 402, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 426, 403, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 427, 404, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 428, 405, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 429, 406, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 430, 407, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 431, 408, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 432, 409, 5)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 433, 410, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 434, 411, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 435, 412, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 436, 413, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 437, 414, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 438, 415, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 439, 416, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 440, 417, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 441, 418, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 442, 419, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 443, 420, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 444, 421, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 445, 422, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 446, 423, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 447, 424, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 448, 425, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 449, 426, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 450, 427, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 451, 428, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 452, 429, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 453, 430, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 454, 431, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 455, 432, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 456, 433, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 457, 434, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'hse_pop3', 458, 435, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 459, 436, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 460, 437, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop5', 461, 438, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 462, 439, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 463, 440, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 464, 441, 3)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'ohc_pop4', 465, 442, 4)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 466, 443, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 467, 444, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 468, 445, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 469, 446, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 470, 447, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 471, 448, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 472, 449, 2)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 473, 450, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'', 474, 451, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 475, 452, 1)
INSERT [dbo].[tbl_acc_popups] ([popup], [masterid], [rowid], [pcount]) VALUES (N'admin_pop1', 476, 453, 1)
SET IDENTITY_INSERT [dbo].[tbl_acc_popups] OFF
SET IDENTITY_INSERT [dbo].[tbl_analytics] ON 

INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (27, N'Admin', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'04-30-2021 03:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (28, N'CMA', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'04-30-2021 03:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (29, N'CMA', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'04-30-2021 03:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (30, N'CMA', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'04-30-2021 03:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (31, N'CMA', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'04-30-2021 03:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (32, N'CMA', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'4/30/2021 8:21:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (33, N'CMA', NULL, 0, NULL, NULL, 15, 0, 2, 2, N'4/30/2021 12:46:21 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (34, N'CMA', NULL, 0, NULL, NULL, 15, 0, 2, 2, N'4/30/2021 1:11:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (35, N'CMA', NULL, 0, NULL, NULL, 15, 0, 2, 2, N'4/30/2021 1:51:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (36, N'CMA', NULL, 0, NULL, NULL, 8, 0, 2, 2, N'4/30/2021 2:02:48 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (37, N'CMA', NULL, 0, NULL, NULL, 15, 0, 2, 2, N'4/30/2021 2:03:49 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (38, N'CMA', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 2:17:55 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (39, N'CMA', NULL, 0, NULL, NULL, 20, 0, 2, 2, N'4/30/2021 2:21:36 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (40, N'CMA', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 3:52:05 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (41, N'CMA', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 3:54:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (42, N'OHC', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 4:00:52 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (43, N'OHC', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 4:01:05 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (44, N'OHC', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 4:01:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (45, N'OHC', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 4:01:22 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (46, N'CMA', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'4/30/2021 4:20:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (47, N'CMA', NULL, 0, NULL, NULL, 15, 0, 2, 2, N'5/3/2021 12:53:58 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (48, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/7/2021 11:01:00 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (49, N'Engineering Store', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/7/2021 11:04:34 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (50, N'Engineering Store', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/7/2021 11:04:35 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (51, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/7/2021 11:08:37 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (52, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/7/2021 11:17:47 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (53, N'CMA', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'5/7/2021 2:18:07 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (54, N'CMA', NULL, 0, NULL, NULL, 18, 0, 2, 2, N'5/7/2021 2:18:07 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (55, N'CMA', NULL, 0, NULL, NULL, 23, 0, 2, 2, N'5/7/2021 2:53:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (56, N'CMA', NULL, 0, NULL, NULL, 23, 0, 2, 2, N'5/7/2021 2:53:22 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (57, N'CMA', NULL, 0, NULL, NULL, 23, 0, 2, 2, N'5/7/2021 2:53:29 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (58, N'Qc/AIMI', NULL, 0, NULL, NULL, 23, 0, 2, 2, N'5/7/2021 2:54:29 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (59, N'Qc/AIMI', NULL, 0, NULL, NULL, 23, 0, 2, 2, N'5/7/2021 2:54:34 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (60, N'CMA', NULL, 0, NULL, NULL, 25, 0, 2, 2, N'5/7/2021 4:09:32 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (61, N'CMA', NULL, 0, NULL, NULL, 25, 0, 2, 2, N'5/10/2021 10:06:46 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (62, N'CMA', NULL, 0, NULL, NULL, 25, 0, 2, 2, N'5/10/2021 10:06:46 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (63, N'Admin', NULL, 0, NULL, NULL, 25, 0, 2, 2, N'5/10/2021 10:19:01 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (64, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 12:39:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (65, N'Admin', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 1:02:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (66, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 1:26:28 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (67, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 2:29:32 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (68, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:34:02 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (69, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:36:55 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (70, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:16 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (71, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (72, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:21 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (73, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:43 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (74, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:49 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (75, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:51 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (76, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:55 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (77, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:37:56 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (78, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:38:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (79, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:38:15 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (80, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:38:41 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (81, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:40:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (82, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:40:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (83, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:41:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (84, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:41:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (85, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:50:38 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (86, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:51:01 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (87, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:51:32 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (88, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:51:43 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (89, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:51:58 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (90, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:53:07 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (91, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:53:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (92, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:53:17 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (93, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:53:42 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (94, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:54:34 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (95, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:54:52 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (96, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:55:08 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (97, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:55:15 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (98, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:55:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (99, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:55:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (100, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:55:58 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (101, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:56:10 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (102, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:57:07 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (103, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:57:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (104, N'OHC', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 3:58:02 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (105, N'Qc/AIMI', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 4:02:28 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (106, N'Qc/AIMI', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/10/2021 4:02:32 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (107, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/11/2021 12:38:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (108, N'Admin', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/11/2021 12:39:44 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (109, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/11/2021 12:42:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (110, N'CMA', NULL, 0, NULL, NULL, 22, 0, 2, 2, N'5/14/2021 2:46:47 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (111, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 1:17:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (112, N'Admin', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 1:21:37 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (113, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 1:24:17 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (114, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 1:24:30 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (115, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 1:25:01 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (116, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 1:25:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (117, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 3:39:09 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (118, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/18/2021 3:52:21 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (119, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/19/2021 4:12:34 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (120, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/19/2021 4:14:14 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (121, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/19/2021 4:14:16 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (122, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/19/2021 4:16:11 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (123, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/19/2021 4:16:14 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (124, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/20/2021 1:36:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (125, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/20/2021 1:38:35 PM')
GO
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (126, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 2:02:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (127, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 2:11:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (128, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 2:12:31 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (129, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 2:17:11 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (130, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/20/2021 3:39:49 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (131, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 6:20:10 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (132, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 7:31:42 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (133, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 9:53:22 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (134, N'Admin', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 9:56:53 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (135, N'Admin', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 9:59:27 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (136, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 10:04:03 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (137, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 10:35:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (138, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/20/2021 10:39:04 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (139, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/21/2021 8:41:51 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (140, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/21/2021 9:28:48 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (141, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/21/2021 9:29:42 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (142, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/24/2021 1:39:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (143, N'Admin', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/24/2021 1:39:31 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (144, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/24/2021 1:45:38 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (145, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/24/2021 1:48:34 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (146, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'5/24/2021 1:48:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (147, N'CMA', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/24/2021 1:57:07 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (148, N'Admin', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/24/2021 3:22:46 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (149, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 1:34:04 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (150, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 1:34:11 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (151, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 1:34:11 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (152, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 1:35:27 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (153, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 1:37:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (154, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 1:38:43 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (155, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 4:58:48 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (156, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 4:58:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (157, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 5:01:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (158, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/25/2021 5:01:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (159, N'Qc/AIMI', NULL, 0, NULL, NULL, 31, 0, 2, 2, N'5/26/2021 9:28:30 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (160, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/22/2021 9:27:56 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (161, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/22/2021 9:27:59 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (162, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/22/2021 9:28:04 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (163, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:38:46 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (164, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:44:38 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (165, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:44:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (166, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:44:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (167, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:44:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (168, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:45:43 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (169, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:45:46 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (170, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:46:48 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (171, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:46:59 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (172, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:47:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (173, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:47:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (174, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:47:59 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (175, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:48:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (176, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:48:21 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (177, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:48:25 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (178, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'6/23/2021 2:48:30 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (179, N'Admin', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/28/2021 5:09:56 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (180, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/28/2021 5:16:34 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (181, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/28/2021 5:16:40 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (182, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 2, 2, N'6/30/2021 2:39:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (183, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 11:59:56 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (184, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:06:47 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (185, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:06:47 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (186, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:06:47 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (187, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:06:47 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (188, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:11:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (189, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:11:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (190, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:11:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (191, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:11:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (192, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:16:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (193, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:16:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (194, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/1/2021 12:17:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (195, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:04:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (196, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:04:54 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (197, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:04:54 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (198, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:04:55 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (199, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:05:03 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (200, N'Admin', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:05:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (201, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:12:29 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (202, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:12:29 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (203, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:20:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (204, N'Engineering Store', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:22:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (205, N'Engineering Store', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:22:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (206, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:26:16 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (207, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:26:55 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (208, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:27:46 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (209, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:28:15 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (210, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:28:40 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (211, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:28:54 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (212, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:29:11 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (213, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:30:07 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (214, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:34:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (215, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:34:59 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (216, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:35:18 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (217, N'CMA', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 3:43:03 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (218, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:13:30 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (219, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:14:15 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (220, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:15:26 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (221, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:15:38 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (222, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:21:02 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (223, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:21:02 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (224, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:21:21 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (225, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:21:34 PM')
GO
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (226, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:21:56 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (227, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:22:47 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (228, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:22:48 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (229, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:22:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (230, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:26:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (231, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:26:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (232, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:26:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (233, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:26:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (234, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:26:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (235, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/2/2021 4:26:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (236, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:28:28 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (237, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:28:43 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (238, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:33:22 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (239, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:33:42 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (240, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:39:14 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (241, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:39:15 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (242, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:47:49 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (243, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:47:49 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (244, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:47:53 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (245, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:48:02 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (246, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:48:06 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (247, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:48:49 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (248, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:49:31 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (249, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:49:39 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (250, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:51:10 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (251, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:51:34 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (252, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:53:15 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (253, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:53:22 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (254, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:53:34 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (255, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:54:12 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (256, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:55:47 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (257, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:55:52 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (258, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:57:37 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (259, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:58:08 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (260, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:58:31 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (261, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:58:36 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (262, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:59:14 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (263, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 10:59:42 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (264, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:00:02 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (265, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:00:38 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (266, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:01:11 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (267, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:02:19 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (268, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:02:36 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (269, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:03:01 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (270, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:03:14 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (271, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:03:33 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (272, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:04:00 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (273, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/5/2021 11:04:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (274, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:35:45 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (275, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:35:58 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (276, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:05 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (277, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:12 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (278, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:16 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (279, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:18 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (280, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:32 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (281, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:36 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (282, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:42 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (283, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:36:49 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (284, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:38:24 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (285, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:38:33 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (286, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:38:38 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (287, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:38:47 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (288, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:38:58 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (289, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:39:03 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (290, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:39:08 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (291, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:39:23 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (292, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:40:12 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (293, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:40:24 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (294, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:40:27 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (295, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:40:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (296, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 2, N'7/13/2021 10:40:53 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (297, N'CMA', NULL, 0, NULL, NULL, 45, 0, 0, 49, N'7/29/2021 9:15:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (298, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 25, N'8/6/2021 3:43:34 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (299, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 29, N'8/6/2021 3:43:38 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (300, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 22, N'8/8/2021 11:10:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (301, N'HSE', NULL, 0, NULL, NULL, 45, 0, 1, 31, N'8/8/2021 11:11:31 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (302, N'CMA', NULL, 0, NULL, NULL, 29, 0, 3, 50, N'9/18/2021 1:53:02 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (303, N'CMA', NULL, 0, NULL, NULL, 29, 0, 3, 55, N'9/18/2021 1:53:06 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (304, N'CMA', NULL, 0, NULL, NULL, 29, 0, 3, 58, N'9/18/2021 1:53:09 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (305, N'CMA', NULL, 0, NULL, NULL, 29, 0, 5, 20, N'9/18/2021 1:54:31 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (306, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 11, N'9/18/2021 1:58:10 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (307, N'Admin', NULL, 0, NULL, NULL, 29, 0, 1, 4, N'9/20/2021 10:44:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (308, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 3, 3, N'9/20/2021 10:49:41 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (309, N'CMA', NULL, 0, NULL, NULL, 29, 0, 1, 57, N'9/20/2021 10:59:29 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (310, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 20, N'9/20/2021 11:04:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (311, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 23, N'9/20/2021 11:04:26 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (312, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 41, N'9/20/2021 11:04:44 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (313, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 29, N'9/20/2021 11:05:32 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (314, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 24, N'9/20/2021 11:06:43 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (315, N'OHC', NULL, 0, NULL, NULL, 29, 0, 1, 20, N'9/20/2021 11:07:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (316, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 0, 11, N'9/20/2021 11:08:13 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (317, N'Admin', NULL, 0, NULL, NULL, 45, 0, 0, 0, N'11/16/2021 8:34:57 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (318, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 0, N'11/16/2021 9:17:44 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (319, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 34, N'11/22/2021 9:42:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (320, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 3, 2, N'11/22/2021 9:49:53 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (321, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 37, N'11/22/2021 9:54:38 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (322, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 56, N'11/22/2021 9:54:56 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (323, N'OHC', NULL, 0, NULL, NULL, 29, 0, 1, 32, N'11/22/2021 9:55:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (324, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 9, N'11/25/2021 1:02:42 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (325, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 14, N'11/25/2021 1:02:48 AM')
GO
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (326, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 11, N'11/26/2021 8:58:37 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (327, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 27, N'11/26/2021 8:58:53 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (328, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 41, N'11/26/2021 8:59:08 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (329, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 45, N'11/26/2021 8:59:12 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (330, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 47, N'11/26/2021 8:59:14 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (331, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 56, N'11/26/2021 8:59:23 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (332, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 4, N'11/26/2021 8:59:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (333, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 27, N'11/26/2021 9:00:02 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (334, N'Admin', NULL, 0, NULL, NULL, 45, 0, 0, 5, N'11/26/2021 9:00:21 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (335, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 18, N'11/26/2021 10:09:52 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (336, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 19, N'11/26/2021 10:09:52 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (337, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 55, N'11/26/2021 10:10:26 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (338, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 12, N'11/26/2021 10:20:38 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (339, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 30, N'11/26/2021 10:20:49 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (340, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 22, N'11/28/2021 9:25:48 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (341, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 42, N'11/28/2021 9:26:09 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (342, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 42, N'11/28/2021 9:27:09 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (343, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 52, N'11/28/2021 9:27:19 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (344, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 7, N'11/28/2021 10:21:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (345, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 13, N'11/28/2021 10:21:56 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (346, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 26, N'11/28/2021 10:22:09 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (347, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 37, N'12/1/2021 1:09:24 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (348, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 0, 4, N'12/1/2021 1:17:56 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (349, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 0, 55, N'12/1/2021 1:18:48 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (350, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 1, 5, N'12/1/2021 1:18:57 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (351, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 3, 16, N'12/1/2021 1:21:08 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (352, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 3, 19, N'12/1/2021 1:21:11 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (353, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 3, 21, N'12/1/2021 1:21:13 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (354, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 3, 33, N'12/1/2021 1:21:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (355, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 3, 59, N'12/1/2021 1:21:51 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (356, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 6, 53, N'12/1/2021 1:24:46 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (357, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 7, 3, N'12/1/2021 1:24:55 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (358, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 8, 45, N'12/1/2021 1:26:38 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (359, N'OHC', NULL, 0, NULL, NULL, 29, 0, 2, 42, N'12/1/2021 1:31:41 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (360, N'OHC', NULL, 0, NULL, NULL, 29, 0, 3, 14, N'12/1/2021 1:32:13 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (361, N'OHC', NULL, 0, NULL, NULL, 29, 0, 3, 32, N'12/1/2021 1:32:31 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (362, N'OHC', NULL, 0, NULL, NULL, 29, 0, 3, 56, N'12/1/2021 1:32:55 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (363, N'OHC', NULL, 0, NULL, NULL, 29, 0, 4, 23, N'12/1/2021 1:33:22 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (364, N'OHC', NULL, 0, NULL, NULL, 29, 0, 4, 52, N'12/1/2021 1:33:51 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (365, N'OHC', NULL, 0, NULL, NULL, 29, 0, 5, 5, N'12/1/2021 1:34:04 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (366, N'OHC', NULL, 0, NULL, NULL, 29, 0, 5, 27, N'12/1/2021 1:34:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (367, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 24, N'12/1/2021 1:35:00 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (368, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 12, N'12/1/2021 1:35:48 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (369, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 19, N'12/1/2021 1:35:54 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (370, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 44, N'12/1/2021 1:36:20 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (371, N'HSE', NULL, 0, NULL, NULL, 29, 0, 1, 52, N'12/1/2021 1:36:28 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (372, N'HSE', NULL, 0, NULL, NULL, 29, 0, 2, 42, N'12/1/2021 1:37:18 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (373, N'HSE', NULL, 0, NULL, NULL, 29, 0, 2, 48, N'12/1/2021 1:37:23 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (374, N'HSE', NULL, 0, NULL, NULL, 29, 0, 2, 51, N'12/1/2021 1:37:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (375, N'HSE', NULL, 0, NULL, NULL, 29, 0, 3, 8, N'12/1/2021 1:37:43 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (376, N'HSE', NULL, 0, NULL, NULL, 29, 0, 3, 49, N'12/1/2021 1:38:25 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (377, N'HSE', NULL, 0, NULL, NULL, 29, 0, 3, 53, N'12/1/2021 1:38:29 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (378, N'HSE', NULL, 0, NULL, NULL, 29, 0, 4, 47, N'12/1/2021 1:39:23 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (379, N'HSE', NULL, 0, NULL, NULL, 29, 0, 4, 51, N'12/1/2021 1:39:27 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (380, N'HSE', NULL, 0, NULL, NULL, 29, 0, 4, 59, N'12/1/2021 1:39:35 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (381, N'HSE', NULL, 0, NULL, NULL, 29, 0, 5, 8, N'12/1/2021 1:39:44 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (382, N'HSE', NULL, 0, NULL, NULL, 29, 0, 6, 53, N'12/1/2021 1:41:28 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (383, N'HSE', NULL, 0, NULL, NULL, 29, 0, 7, 7, N'12/1/2021 1:41:43 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (384, N'HSE', NULL, 0, NULL, NULL, 29, 0, 7, 9, N'12/1/2021 1:41:45 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (385, N'HSE', NULL, 0, NULL, NULL, 29, 0, 7, 12, N'12/1/2021 1:41:48 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (386, N'HSE', NULL, 0, NULL, NULL, 29, 0, 7, 14, N'12/1/2021 1:41:50 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (387, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 8, N'12/1/2021 1:45:27 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (388, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 13, N'12/1/2021 1:45:31 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (389, N'CMA', NULL, 0, NULL, NULL, 29, 0, 2, 16, N'12/1/2021 1:45:34 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (390, N'Canteen', NULL, 0, NULL, NULL, 29, 0, 5, 32, N'12/1/2021 2:01:32 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (391, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 9, N'12/3/2021 9:57:11 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (392, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 0, 4, N'12/3/2021 9:57:58 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (393, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 10, N'12/3/2021 9:58:27 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (394, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 11, N'12/3/2021 9:58:29 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (395, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 15, N'12/3/2021 9:58:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (396, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 17, N'12/3/2021 9:58:35 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (397, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 53, N'12/8/2021 2:04:20 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (398, N'OHC', NULL, 0, NULL, NULL, 29, 0, 0, 59, N'12/8/2021 2:04:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (399, N'Admin', NULL, 0, NULL, NULL, 45, 0, 0, 0, N'12/8/2021 9:28:25 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (400, N'Admin', NULL, 0, NULL, NULL, 45, 0, 0, 0, N'12/8/2021 9:28:42 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (401, N'CMA', NULL, 0, NULL, NULL, 45, 0, 1, 24, N'12/8/2021 9:31:20 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (402, N'CMA', NULL, 0, NULL, NULL, 45, 0, 1, 27, N'12/8/2021 9:31:22 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (403, N'HSE', NULL, 0, NULL, NULL, 29, 0, 0, 24, N'12/17/2021 1:05:17 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (404, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 24, 19, N'1/5/2022 2:02:48 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (405, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 34, 1, N'1/5/2022 2:12:27 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (406, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 34, 5, N'1/5/2022 2:12:27 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (407, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 7, N'1/5/2022 2:16:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (408, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 12, N'1/5/2022 2:16:42 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (409, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 26, N'1/5/2022 2:16:56 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (410, N'OHC', NULL, 0, NULL, NULL, 45, 0, 2, 50, N'1/5/2022 2:17:20 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (411, N'OHC', NULL, 0, NULL, NULL, 45, 0, 3, 43, N'1/5/2022 2:18:14 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (412, N'OHC', NULL, 0, NULL, NULL, 45, 0, 3, 56, N'1/5/2022 2:18:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (413, N'OHC', NULL, 0, NULL, NULL, 45, 0, 4, 21, N'1/5/2022 2:18:51 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (414, N'OHC', NULL, 0, NULL, NULL, 45, 0, 4, 50, N'1/5/2022 2:19:21 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (415, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 10, N'1/5/2022 2:19:38 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (416, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 56, N'1/5/2022 2:22:29 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (417, N'HSE', NULL, 0, NULL, NULL, 45, 0, 2, 59, N'1/5/2022 2:22:29 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (418, N'HSE', NULL, 0, NULL, NULL, 45, 0, 3, 41, N'1/5/2022 2:23:09 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (419, N'HSE', NULL, 0, NULL, NULL, 45, 0, 3, 58, N'1/5/2022 2:23:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (420, N'HSE', NULL, 0, NULL, NULL, 45, 0, 5, 39, N'1/5/2022 2:25:08 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (421, N'HSE', NULL, 0, NULL, NULL, 45, 0, 5, 42, N'1/5/2022 2:25:11 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (422, N'HSE', NULL, 0, NULL, NULL, 45, 0, 5, 52, N'1/5/2022 2:25:21 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (423, N'HSE', NULL, 0, NULL, NULL, 45, 0, 5, 55, N'1/5/2022 2:25:24 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (424, N'HSE', NULL, 0, NULL, NULL, 45, 0, 6, 1, N'1/5/2022 2:25:30 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (425, N'HSE', NULL, 0, NULL, NULL, 45, 0, 6, 23, N'1/5/2022 2:25:52 AM')
GO
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (426, N'CMA', NULL, 0, NULL, NULL, 45, 0, 0, 54, N'1/5/2022 2:31:35 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (427, N'CMA', NULL, 0, NULL, NULL, 45, 0, 2, 51, N'1/5/2022 2:33:25 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (428, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 0, 25, N'1/5/2022 2:45:56 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (429, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 0, 27, N'1/5/2022 2:45:58 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (430, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 0, 50, N'1/5/2022 2:46:21 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (431, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 1, 34, N'1/5/2022 2:47:06 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (432, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 4, 19, N'1/5/2022 2:49:50 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (433, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 56, N'1/13/2022 1:09:55 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (434, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 21, N'1/18/2022 1:33:05 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (435, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 31, N'1/18/2022 1:33:14 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (436, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 34, N'1/18/2022 1:33:18 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (437, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 37, N'1/18/2022 1:33:20 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (438, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 40, N'1/18/2022 1:33:24 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (439, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 46, N'1/18/2022 1:33:29 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (440, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 57, N'1/18/2022 1:33:40 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (441, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 1, 1, N'1/18/2022 1:33:44 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (442, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 36, N'1/20/2022 5:16:15 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (443, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 0, 10, N'1/20/2022 5:18:28 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (444, N'Qc/AIMI', NULL, 0, NULL, NULL, 29, 0, 0, 48, N'1/20/2022 5:19:06 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (445, N'Admin', NULL, 0, NULL, NULL, 29, 0, 8, 14, N'1/20/2022 5:31:26 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (446, N'Admin', NULL, 0, NULL, NULL, 29, 0, 8, 17, N'1/20/2022 5:31:28 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (447, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 18, N'1/21/2022 10:15:03 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (448, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 14, N'1/21/2022 10:15:57 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (449, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 20, N'1/21/2022 10:16:04 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (450, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 23, N'1/21/2022 10:16:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (451, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 26, N'1/21/2022 10:16:09 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (452, N'HSE', NULL, 0, NULL, NULL, 45, 0, 1, 47, N'1/21/2022 10:17:31 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (453, N'HSE', NULL, 0, NULL, NULL, 45, 0, 1, 48, N'1/21/2022 10:17:31 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (454, N'HSE', NULL, 0, NULL, NULL, 45, 0, 1, 56, N'1/21/2022 10:17:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (455, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 7, N'1/21/2022 10:18:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (456, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 39, N'1/21/2022 10:18:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (457, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 7, N'1/21/2022 10:23:50 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (458, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 10, N'1/21/2022 10:23:53 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (459, N'HSE', NULL, 0, NULL, NULL, 45, 0, 0, 55, N'1/21/2022 10:24:39 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (460, N'HSE', NULL, 0, NULL, NULL, 45, 0, 1, 1, N'1/21/2022 10:24:44 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (461, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 7, N'1/21/2022 10:25:33 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (462, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 11, N'1/21/2022 10:25:37 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (463, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 17, N'1/21/2022 10:25:42 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (464, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 18, N'1/21/2022 10:25:44 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (465, N'OHC', NULL, 0, NULL, NULL, 45, 0, 0, 21, N'1/21/2022 10:25:46 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (466, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 0, N'1/26/2022 8:35:49 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (467, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 0, N'1/26/2022 8:35:59 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (468, N'Canteen', NULL, 0, NULL, NULL, 45, 0, 0, 52, N'1/26/2022 8:37:48 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (469, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 5, N'1/26/2022 8:38:00 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (470, N'Qc/AIMI', NULL, 0, NULL, NULL, 45, 0, 0, 7, N'1/26/2022 8:38:02 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (471, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 0, 22, N'1/26/2022 8:39:05 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (472, N'Engineering Store', NULL, 0, NULL, NULL, 29, 0, 0, 24, N'1/26/2022 8:39:06 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (473, N'Admin', NULL, 0, NULL, NULL, 29, 0, 7, 52, N'1/26/2022 8:54:12 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (474, N'Admin', NULL, 0, NULL, NULL, 29, 0, 7, 54, N'1/26/2022 8:54:14 PM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (475, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 33, N'1/27/2022 1:16:35 AM')
INSERT [dbo].[tbl_analytics] ([aid], [module], [Count], [k_Time_days], [p_accessed], [Pop_Ups], [userid], [k_Time_hours], [k_Time_min], [k_Time_sec], [Addeddate]) VALUES (476, N'Admin', NULL, 0, NULL, NULL, 29, 0, 0, 49, N'1/27/2022 1:19:05 AM')
SET IDENTITY_INSERT [dbo].[tbl_analytics] OFF
SET IDENTITY_INSERT [dbo].[tbl_app_links] ON 

INSERT [dbo].[tbl_app_links] ([Linkid], [LinkTitle], [LinkUrl]) VALUES (1, N'VT Mobile Application', N'http://10.26.1.73/VirtualTour/AnroidApp/Bayer_VT_UATv01.apk')
INSERT [dbo].[tbl_app_links] ([Linkid], [LinkTitle], [LinkUrl]) VALUES (2, N'VT HMD Application', N'www.facebook.com')
INSERT [dbo].[tbl_app_links] ([Linkid], [LinkTitle], [LinkUrl]) VALUES (3, N'VT Desktop Application', N'http://10.26.1.73/\/irtualTour/Desktop/Bayer_UAT_Version0.1.zip')
SET IDENTITY_INSERT [dbo].[tbl_app_links] OFF
SET IDENTITY_INSERT [dbo].[tbl_assign_pages] ON 

INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (30, 10, 7)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (31, 10, 8)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (41, 8, 2)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (42, 8, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (43, 4, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (44, 4, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (45, 4, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (46, 1, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (47, 1, 2)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (48, 1, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (49, 1, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (50, 1, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (51, 1, 6)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (52, 1, 7)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (53, 1, 8)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (54, 1, 9)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (55, 5, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (56, 5, 2)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (57, 5, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (58, 5, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (59, 5, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (60, 5, 6)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (61, 5, 9)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (62, 1, 10)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (63, 9, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (64, 9, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (65, 9, 5)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (66, 9, 6)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (70, 12, 1)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (71, 12, 3)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (72, 12, 4)
INSERT [dbo].[tbl_assign_pages] ([aid], [roleid], [pageid]) VALUES (73, 12, 6)
SET IDENTITY_INSERT [dbo].[tbl_assign_pages] OFF
SET IDENTITY_INSERT [dbo].[tbl_Error_Log] ON 

INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (1, N'An error occurred while executing the command definition. See the inner exception for details.', N'GetUser', N'LoginMaster', CAST(N'2021-04-14T23:40:35.800' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (2, N'An error occurred while executing the command definition. See the inner exception for details.', N'GetUser', N'LoginMaster', CAST(N'2021-04-14T23:41:09.203' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (3, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:35:58.157' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (4, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:36:17.060' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (5, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:41:42.427' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (6, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:50:42.613' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (7, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:50:43.107' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (8, N'System.Data.Entity.Core.EntityCommandExecutionException: An error occurred while executing the command definition. See the inner exception for details. ---> System.Data.SqlClient.SqlException: Cannot find either column "dbo" or the user-defined function or aggregate "dbo.ModulesName", or the name is ambiguous.
   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption, Boolean shouldCacheForAlwaysEncrypted)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.<Reader>b__c(DbCommand t, DbCommandInterceptionContext`1 c)
   at System.Data.Entity.Infrastructure.Interception.InternalDispatcher`1.Dispatch[TTarget,TInterceptionContext,TResult](TTarget target, Func`3 operation, TInterceptionContext interceptionContext, Action`3 executing, Action`3 executed)
   at System.Data.Entity.Infrastructure.Interception.DbCommandDispatcher.Reader(DbCommand command, DbCommandInterceptionContext interceptionContext)
   at System.Data.Entity.Internal.InterceptableDbCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   --- End of inner exception stack trace ---
   at System.Data.Entity.Core.EntityClient.Internal.EntityCommandDefinition.ExecuteStoreCommands(EntityCommand entityCommand, CommandBehavior behavior)
   at System.Data.Entity.Core.Objects.ObjectContext.CreateFunctionObjectResult[TElement](EntityCommand entityCommand, ReadOnlyCollection`1 entitySets, EdmType[] edmTypes, ExecutionOptions executionOptions)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__46()
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteInTransaction[T](Func`1 func, IDbExecutionStrategy executionStrategy, Boolean startLocalTransaction, Boolean releaseConnectionOnSuccess)
   at System.Data.Entity.Core.Objects.ObjectContext.<>c__DisplayClass47`1.<ExecuteFunction>b__45()
   at System.Data.Entity.SqlServer.DefaultSqlExecutionStrategy.Execute[TResult](Func`1 operation)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ExecutionOptions executionOptions, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, MergeOption mergeOption, ObjectParameter[] parameters)
   at System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction[TElement](String functionName, ObjectParameter[] parameters)
   at webapp_ver1.DataEntities.GetAssignedData(Nullable`1 roleid) in F:\wwwroot\Ver_model.Context.cs:line 49
   at webapp_ver1.Controllers.DataApisController.Login() in F:\wwwroot\Controllers\DataApisController.cs:line 139', N'DataApis', N'Login', CAST(N'2021-04-17T04:56:50.737' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (9, N'The server could not be contacted.', N'LoginController', N'LoginMaster', CAST(N'2021-11-14T11:54:02.210' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (10, N'The LDAP server is unavailable.', N'LoginController', N'LoginMaster', CAST(N'2021-11-15T23:21:21.597' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (11, N'The LDAP server is unavailable.', N'LoginController', N'LoginMaster', CAST(N'2021-11-15T23:24:43.620' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (12, N'SOme error Occur.Please try again', N'LoginController', N'LoginMaster', CAST(N'2021-11-15T23:29:59.913' AS DateTime))
INSERT [dbo].[tbl_Error_Log] ([Id], [Message], [Method_Name], [Page_Name], [date]) VALUES (13, N'SOme error Occur.Please try again', N'LoginController', N'LoginMaster', CAST(N'2021-11-15T23:31:31.960' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Error_Log] OFF
SET IDENTITY_INSERT [dbo].[tbl_pages] ON 

INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (2, N'Qc/AIMI', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (3, N'Engineering Store', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (4, N'OHC', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (5, N'HSE', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (6, N'Canteen', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (7, N'GMP', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (8, N'CMA', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (9, N'Vr Road Tour', 1)
INSERT [dbo].[tbl_pages] ([pid], [Page_Name], [Isactive]) VALUES (10, N'Warehouse', 1)
SET IDENTITY_INSERT [dbo].[tbl_pages] OFF
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop1', N'Leed Certificate', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop2', N'Kiosk', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop3', N'Lay / Malhar conference room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop4 ', N'The cafeteria', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'admin_pop5', N'The longue/ Santoor Conference room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop1', N'HPLC - ', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop2 ', N' Air Genertaor



 
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop3', N'Left inst (3 big one) - Nitrogen Generator', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop4', N'Auto tritrator', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop5', N'UV spectrometer', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop6', N'Density meter', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop7', N'microscope', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop8', N'GCMS -Gas Chromotograph with Mass spectrometer', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop9', N'GCHS - GC Head Space', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop10', N'Printer type inst.- auto circulator for controoling the temp at auto reactor', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop11', N'20L Jacketed Reactor
10L Jacketed Reactor
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop12', N'Centrifuge for Acidic / Alkaline filtration', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop13', N'Auto reactor -', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop14', N'Hydrogen generator
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop15', N'Auto circulator for controlling the temperature at autoreactor
', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop16', N'High pressure reactor', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'qc_pop17', N'Millipore: - The most advanced pure water system
•           Weighing Balance: - METTLER TOLEDO Advanced Analytical Balance
•           HPLC: - High performance liquid chromatography
•           Mastersizer: - Particle size analyzer
•           Kinexus: -', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'canteen_pop1', N'RFID sensor for capturing Lunch & Dinner ', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'canteen_pop2', N'. Roti making area, 2. cooking area. 3. Tea making area 4. Dish wash area. 5 Salad making area', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'canteen_pop3', N'rice boiler, dishwasher machine, dough kneader machine, palversier machine, Grinding machine, Potato pillar machine idli making machine, cold and dry storage.', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'Eng_pop1', N'Total Area of Engineering Spare part warehouse is 50.5 X 25.5 X 10 meters(LBH) (1250 Sq.mtr', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'Eng_pop2', N'Aisle in which  Crane travel on rail track to pick and store the Storage Bin.
- Shuttle car is the vehicle who delivers the bin received from crane to IN / OUT station through conveyors. shuttle car is with PLC system and integrated with WMS.
- Cranes', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'Eng_pop3', N'The Crane travels on rail track to pick or to store the bins.
- Shuttle car is the vehicle which delivers the bin received from crane to IN / OUT station through conveyors. Shuttle car is also connected to the PLC system and integrated with WMS.', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop1', N'Audiometry Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop2', N'AED Machine', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop3', N'Suction Machine - OHC/  Ambulance', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop4', N'Oxygen Cylinder - OHC/  Ambulance/ Plants', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop5', N'ECG Machine - OHC Recovery Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop6', N'Needle Syringe Destroyer - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop7', N'Instrument Sterilizer - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop8', N'Autoclave Machine - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'ohc_pop9', N'Water Distiller - OHC Equipment Room', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop1', N'Cardiac Monitor - Ambulance', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop2', N'Autoloded Stretcher', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop3', N'Spinal Stretcher ', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'amb_pop4', N'WheelChair', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop1', N'Fire Hosepipes', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop2', N'75 kg tank Dry-chemical powder', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop3', N'2 Nos. 22.5 kg CO2 extinguisher', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop4', N'Fog branch & Jumbo', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop5', N'fire-bucket', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop6', N'Fireman Axe', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop7', N'SCBA-Set', NULL, NULL, NULL)
INSERT [dbo].[tbl_popups] ([ID], [PopupName], [F3], [F4], [F5]) VALUES (N'hse_pop8', N'Microchem suit 3000 & 5000', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_questions] ON 

INSERT [dbo].[tbl_questions] ([Qid], [Question]) VALUES (1, N'Rate our App')
SET IDENTITY_INSERT [dbo].[tbl_questions] OFF
SET IDENTITY_INSERT [dbo].[tbl_ratings] ON 

INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (1, 12, 5, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (2, 12, 4, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (3, 12, 3, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (4, 12, 3, 1, NULL)
INSERT [dbo].[tbl_ratings] ([Rid], [userid], [rating], [Qid], [additional_comment]) VALUES (5, 17, 5, 5, N'this is additional comment')
SET IDENTITY_INSERT [dbo].[tbl_ratings] OFF
SET IDENTITY_INSERT [dbo].[tbl_Role] ON 

INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (4, N'Induction', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (5, N'Internal Bayer Employee', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (6, N'Vendor', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (8, N'Auditor', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (9, N'Student', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (10, N'Marketing Customer', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (11, N'Government officer', 1)
INSERT [dbo].[tbl_Role] ([R_Id], [RoleName], [Isactive]) VALUES (12, N'TCS Team', 1)
SET IDENTITY_INSERT [dbo].[tbl_Role] OFF
SET IDENTITY_INSERT [dbo].[tbl_User] ON 

INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (1, N'Admin', 1, NULL, NULL, NULL, N'nmjn@12#hmk12$lkvnc', NULL, NULL, NULL, NULL, N'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (8, N'Deepak', 2, NULL, N'deepak@airvlabs.com', N'9166474449', NULL, 1, NULL, NULL, N'Singh', NULL, N'cmFMKcZiZPIdBDYycfH+Qw==', CAST(N'2021-04-22T10:43:39.263' AS DateTime), CAST(N'2021-05-16T15:46:18.997' AS DateTime), 0, 10, 5, NULL, 0)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (12, N'mukul', 2, NULL, N'er.mukul09@gmail.com', N'9782316861', N'mkpassword', 1, NULL, NULL, N'sharma', N'mksharma', NULL, CAST(N'2021-04-29T12:58:43.457' AS DateTime), CAST(N'2021-05-09T07:28:44.060' AS DateTime), 10, 0, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (14, N'deepak', 3, NULL, N'deepak491singh@gmail.com', N'9784567654', N'123456', 1, NULL, NULL, NULL, N'dpk', NULL, CAST(N'2021-04-30T00:48:36.187' AS DateTime), CAST(N'2021-04-30T05:18:36.187' AS DateTime), 0, 10, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (17, N'Visitor1', 9, NULL, N'chinmay@airvlabs.com', N'1234567891', N'visitor', 1, NULL, NULL, N'1', N'Visit', N'fnq4tQq+R7uqhs3RLncl+g==', CAST(N'2021-04-30T14:13:09.717' AS DateTime), CAST(N'2021-04-30T08:53:09.717' AS DateTime), 0, 0, 10, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (28, N'kk', 5, NULL, N'gorikaran0@gmail.com', N'1121123444', N'kg123', 1, NULL, NULL, N'gg', N'kg', NULL, CAST(N'2021-05-11T09:52:29.590' AS DateTime), CAST(N'2021-05-31T04:22:29.590' AS DateTime), 20, 0, 0, NULL, NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (29, N'Krunal', 1, NULL, N'krunal.prajapati@bayer.com', N'1122334451', NULL, 1, NULL, NULL, N'Prajapati', NULL, N'm7Q6EVMrNASreJYHcyVZ7g==', CAST(N'2021-05-12T12:07:40.120' AS DateTime), CAST(N'2022-05-12T06:37:40.120' AS DateTime), 365, 0, 0, N'MBPIK', NULL)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (30, N'ssd', 1, NULL, N'e.rmukul09@gmail.com', N'8989898989', NULL, 1, NULL, NULL, N'sdsd', NULL, NULL, CAST(N'2021-05-16T10:48:02.430' AS DateTime), CAST(N'2021-05-26T07:38:53.367' AS DateTime), 10, 0, 0, NULL, 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (32, N'AAAB', 5, NULL, N'AAAB@GMAIL.COM', N'1232343456', NULL, 1, NULL, NULL, N'AAAB', NULL, NULL, CAST(N'2021-05-20T13:32:24.953' AS DateTime), CAST(N'2021-06-04T08:05:02.573' AS DateTime), 15, 0, 0, NULL, 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (33, N'Deepak', 1, NULL, N'deepak.singh3.ext@bayer.com', N'1122112211', NULL, 1, NULL, NULL, N'Singh', NULL, N'Ev5Z0G6JRyBcxEMYjXHKwA==', CAST(N'2021-05-24T11:29:27.040' AS DateTime), CAST(N'2022-05-24T05:59:27.040' AS DateTime), 365, 0, 0, N'EIIMD', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (39, N'Nilesh', 1, NULL, N'nilesh.jain.ext@bayer.com', N'3333333333', NULL, 1, NULL, NULL, N'Jain', NULL, NULL, CAST(N'2021-06-03T19:22:21.067' AS DateTime), CAST(N'2024-09-18T16:16:20.040' AS DateTime), 999, 999, 999, N'EKFWS', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (41, N'Janhavi', 4, NULL, N'janhavi.dewoolkar.ext@bayer.com', N'2225311996', NULL, 1, NULL, NULL, N'Dewoolkar', NULL, NULL, CAST(N'2021-06-03T19:32:55.317' AS DateTime), CAST(N'2021-06-04T14:02:55.317' AS DateTime), 1, 0, 0, N'EMUYV', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (45, N'Karan', 1, NULL, N'karan.gori.ext@bayer.com', N'1121121121', NULL, 1, NULL, NULL, N'Gori', NULL, N'Nfco6jXgIzkaePtNEQEGlw==', CAST(N'2021-06-11T16:07:46.577' AS DateTime), CAST(N'2024-04-02T17:49:55.817' AS DateTime), 893, 10, 0, N'EICYY', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (46, N'HSP', 10, NULL, N'hsp.nagashayanam@bayer.com', N'1212134343', NULL, 1, NULL, NULL, N'Nagashyanam', NULL, N'YrTS66nP+Oa5TSsN8dlhWw==', CAST(N'2021-07-02T15:38:40.187' AS DateTime), CAST(N'2022-07-02T13:33:36.567' AS DateTime), 365, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (47, N'KKK', 5, NULL, N'gori1998karan@gmail.com', N'1234432177', N'KKGGG', 1, NULL, NULL, N'GGG', N'KKKGG', NULL, CAST(N'2021-09-06T22:05:08.297' AS DateTime), CAST(N'2021-09-22T05:05:08.297' AS DateTime), 15, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (48, N'TestUser1', 1, NULL, N'test1@malinator.com', N'1234567890', N'Password', 1, NULL, NULL, NULL, N'TestUser1', N'tMYZJkJ8Kd99p0vR+MjfSg==', CAST(N'2021-10-13T11:02:01.743' AS DateTime), CAST(N'2021-10-23T18:02:01.743' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (49, N'TestUser2', 9, NULL, N'Test2@mailnator.com', N'2345678901', N'Password', 1, NULL, NULL, NULL, N'TestUser2', NULL, CAST(N'2021-10-13T11:03:08.103' AS DateTime), CAST(N'2021-10-23T18:03:08.103' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (50, N'TestUser3', 10, NULL, N'Test3@malinator.com', N'345678901256', N'Password', 1, NULL, NULL, NULL, N'TestUser3', NULL, CAST(N'2021-10-13T11:04:04.580' AS DateTime), CAST(N'2021-10-23T18:04:04.580' AS DateTime), 10, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (51, N'nitin', 1, NULL, N'nitinrvl@gmail.com', N'9876768967', N'NRNR', 1, NULL, NULL, N'raval', N'NR', NULL, CAST(N'2021-11-01T23:04:04.533' AS DateTime), CAST(N'2021-12-05T06:04:04.533' AS DateTime), 33, 0, 0, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (52, N'kkaran', 1, NULL, N'dsknkdsf@gmail.com', N'3874240973', N'kgkgkg', 1, NULL, NULL, N'ggori', N'kgkgkg', N'rOlyk1kZsOdIIJsb30PWEQ==', CAST(N'2021-12-13T23:09:04.433' AS DateTime), CAST(N'2021-12-14T07:14:04.433' AS DateTime), 0, 0, 5, NULL, 2)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (53, N'Mugdha', 1, NULL, N'mugdha.rane.ext@bayer.com', N'1243576890', NULL, 1, NULL, NULL, N'Rane', NULL, NULL, CAST(N'2021-12-28T01:22:12.723' AS DateTime), CAST(N'2022-01-09T09:30:22.670' AS DateTime), 12, 0, 0, N'euphh', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (54, N'Kapil', 1, NULL, N'kapil.gupta1.ext@bayer.com', N'3456789345', NULL, 1, NULL, NULL, N'Gupta', NULL, NULL, CAST(N'2022-01-17T01:22:57.713' AS DateTime), CAST(N'2022-02-21T09:22:57.713' AS DateTime), 35, 0, 0, N'EHFAW', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (55, N'Twinkle', 1, NULL, N'twinkle.vishwakarma.ext@bayer.com', N'8765430123', NULL, 1, NULL, NULL, N'Vishwakarma', NULL, NULL, CAST(N'2022-01-17T01:23:45.517' AS DateTime), CAST(N'2022-03-03T09:23:45.517' AS DateTime), 45, 0, 0, N'EISZA', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (56, N'Saurabh', 1, NULL, N'saurabh.verma.ext@bayer.com', N'8765876543', NULL, 1, NULL, NULL, N'Verma', NULL, NULL, CAST(N'2022-01-17T01:24:32.213' AS DateTime), CAST(N'2022-03-08T09:24:32.213' AS DateTime), 50, 0, 0, N'EMLZG', 1)
INSERT [dbo].[tbl_User] ([Id], [F_Name], [RoleId], [DOB], [EmailId], [Mobile_No], [Password], [Isactive], [OTP], [Role], [L_Name], [UserName], [Token], [addeddate], [_ExpiryTime], [Days], [Hours], [Min], [CWID], [UserType]) VALUES (57, N'Shubham', 1, NULL, N'shubham.jain1.ext@bayer.com', N'9876501234', NULL, 1, NULL, NULL, N'Jain', NULL, NULL, CAST(N'2022-01-17T01:27:16.890' AS DateTime), CAST(N'2022-03-13T09:27:16.890' AS DateTime), 55, 0, 0, N'EILJF', 1)
SET IDENTITY_INSERT [dbo].[tbl_User] OFF
/****** Object:  StoredProcedure [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3].[GetRatingAnalytics]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [AD-BAYER-CNB\ef.a.sql.ap.usr-dev.bysg015cmk8.inpd3].[GetRatingAnalytics]
@qid int
as
begin
select count(rid) 'Count',rating from tbl_ratings where qid=@qid group by rating
end



GO
/****** Object:  StoredProcedure [dbo].[GetAssignedData]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetAssignedData]
@roleid int=null
as
begin
declare @pages nvarchar(max)
if(isnull(@roleid,0)=0)
begin
select distinct(roleid),RoleName,isnull(dbo.[ModulesName](roleid),'') 'Modules' from tbl_assign_pages inner join tbl_pages on pageid=pid inner join tbl_Role on tbl_assign_pages.roleid=tbl_Role.r_id
end
else
begin
select distinct(roleid),RoleName,isnull(dbo.[ModulesName](roleid),'') 'Modules' from tbl_assign_pages inner join tbl_pages on pageid=pid inner join tbl_Role on tbl_assign_pages.roleid=tbl_Role.r_id where r_id=@roleid

end

end

GO
/****** Object:  StoredProcedure [dbo].[GetModuleVisits]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE Proc [dbo].[GetModuleVisits] 
@month int,
@year int  
as  
begin  
declare @totalcount float  
select @totalcount=count(aid)  from tbl_analytics   where  MONTH(cast(tbl_analytics.addeddate as date))=@month and  year(cast(tbl_analytics.addeddate as date))=@year
if(@totalcount>0)  
select Page_Name,convert(float,Round((count(aid)/@totalcount)*100,2) )'Count' from tbl_pages inner join tbl_analytics on module=Page_Name inner join tbl_User on tbl_User.id=userid
 where MONTH(cast(tbl_analytics.addeddate as date))=@month and  year(cast(tbl_analytics.addeddate as date))=@year group by Page_Name  
else  
select '' Page_Name,convert(float,'0') 'Count'   
  

end  

GO
/****** Object:  StoredProcedure [dbo].[GetRatingAnalytics]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetRatingAnalytics]
@qid int
as
begin
select count(rid) 'Count',rating from tbl_ratings group by rating
end
GO
/****** Object:  StoredProcedure [dbo].[getUserGrupedByName]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[getUserGrupedByName] 
@mname int,
@year int
as


begin
select count(id) 'Count',RoleName 'Month' from tbl_user inner join tbl_role on roleid=r_id where month(addeddate)=@mname and year(addeddate)=@year group by RoleName
end
GO
/****** Object:  StoredProcedure [dbo].[GetUserList]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserList]
AS
BEGIN
	select Id,F_Name,RoleId,DOB,EmailId,Mobile_No,Password,tbl_User.Isactive,OTP,cwid 'Role',L_Name,UserName,R_Id,RoleName,tbl_Role.Isactive 'Isactive1',UserType from tbl_User 
	inner join tbl_Role on tbl_Role.R_Id=tbl_User.RoleId
	
END


GO
/****** Object:  StoredProcedure [dbo].[GetVisitedPopups]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetVisitedPopups] 
@aid bigint
as

begin
select popupname from tbl_popups inner join tbl_acc_popups on id=popup where masterid=@aid
end
GO
/****** Object:  StoredProcedure [dbo].[GetVisiting_Details]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[GetVisiting_Details] 
@userid bigint
as

begin
select aid,module,k_Time_days,k_Time_hours,k_Time_min,k_Time_sec,userid,isnull(addeddate,'') 'addeddate' from tbl_analytics where userid=@userid order by addeddate desc
end
GO
/****** Object:  StoredProcedure [dbo].[GetVisitingUsers]    Script Date: 1/29/2022 9:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[GetVisitingUsers]
as
begin
select distinct(userid) 'Id',f_Name,Mobile_No,EmailId,rolename from tbl_analytics inner join tbl_user on id=userid inner join tbl_Role on r_id=roleid
end

GO
USE [master]
GO
ALTER DATABASE [BVPL-VT] SET  READ_WRITE 
GO
